#include <iostream>

using std::cout;

class Vector
{
public:
    Vector()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    Vector(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    friend Vector operator+(const Vector& a, const Vector& b);

    friend Vector operator-(const Vector& a, const Vector& b);

    friend Vector operator*(const Vector& a, float af);

    friend Vector operator/(const Vector& a, float af);

    friend std::ostream& operator<<(std::ostream& out, const Vector& v);
    
    friend std::istream& operator>>(std::istream& in, Vector& v);

private:
    float x;
    float y;
    float z;
};

Vector operator+(const Vector& a, const Vector& b)
{
    return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

Vector operator*(const Vector& a, const float af)
{
    return Vector(a.x * af, a.y * af, a.z * af);
}

Vector operator/(const Vector& a, const float af)
{
    return Vector(a.x / af, a.y / af, a.z / af);
}

std::ostream& operator<<(std::ostream& out, const Vector& v)
{
    out << v.x << " " << v.y << " " << v.z << std::endl;
    return out;
}

Vector operator-(const Vector& a, const Vector& b)
{
    return Vector(a.x - b.x, a.y - b.y, a.z - b.z);
}

std::istream& operator>>(std::istream& in, Vector& mv)
{
    in >> mv.x,
    in >> mv.y,
    in >> mv.z;
    
    return in;
}

int main()
{
    Vector VarOne(0,1,2);
    Vector VarTwo(3,4,5);
    cout << "Vector InOne = " << VarOne;
    cout << "Vector InTwo = " << VarTwo;
    
    Vector VarFour;
    VarFour = VarOne + VarTwo;   
    cout << "*** Add-up vectors = " << VarFour;

    Vector InOne;
    cout << "Enter x,y,z Vector InOne :"; 
    std::cin >> InOne;
    cout << "You Entered: " << InOne;
    
    Vector InTwo;
    cout << "Enter x,y,z Vector InTwo :";
    std::cin >> InTwo;
    cout << "You Entered: " << InTwo;

    Vector InFour;
    InFour = InOne - InTwo;
    cout << " *** Subtraction vectors = " << InFour;

    return 0;
}